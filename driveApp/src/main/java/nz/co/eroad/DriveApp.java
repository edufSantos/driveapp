package nz.co.eroad;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.maps.model.LatLng;

public class DriveApp {
	
	/**
	 * Init the application
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Starting DriveApp v1.0 by Eduardo Santos... \n");
		
		DriveApp app = new DriveApp();
		Path filePath = null;

		try {
			filePath = Paths.get(DriveApp.class.getResource("../../../data.txt").toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		app.readData(filePath);
		
		System.out.println("\n Finishing DriveApp v1.0 by Eduardo Santos... \n");
	}
	
	/**
	 * Reads the file data to send to Google API to be processed
	 * @param filePath
	 */
	private void readData(Path filePath) {
		List<String> fileData = null;

		try {
			fileData = Files.readAllLines(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Function<LatLng, Optional<TimeZone>> coordinatesToTime = new CoordinatesToTime();
		List<String> convertedData = processData(fileData, coordinatesToTime);
		
		//Gives space between google data logged in console and the app data
		System.out.println("\n");
		
		convertedData.forEach(s -> System.out.println(s));
	}
	
	/**
	 * 
	 * @param fileData List<String> containing all data
	 * @param coordinatesToTime Function<LatLng, Optional<TimeZone>> Receives a coordinate e transform it to time zone
	 * @return List<String> containing all processed data
	 */
	private List<String> processData(List<String> fileData, Function<LatLng, Optional<TimeZone>> coordinatesToTime) {
		return fileData.stream().map(input -> {
			final String[] arrData = input.split(",");
			String dateTime = arrData[0];
			
			Double lat = Double.valueOf(arrData[1]);
			Double lng = Double.valueOf(arrData[2]);
			
			LatLng latLng = new LatLng(lat, lng);
			Optional<TimeZone> timeZone = coordinatesToTime.apply(latLng);

			if(!timeZone.isPresent())
				return "Invalid data!";

			LocalDateTime localDateTime = DateUtil.getLocalDateFromUtc(dateTime);
			String formattedDateTime = localDateTime.toString().replace("T", " ");

			final String processedData = input.concat(",").concat(timeZone.get().getID()).concat(",")
					.concat(formattedDateTime);
			return processedData;
		}).collect(Collectors.toList());
	}
}
