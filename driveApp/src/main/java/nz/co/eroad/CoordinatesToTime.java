package nz.co.eroad;

import java.util.Optional;
import java.util.TimeZone;
import java.util.function.Function;

import com.google.maps.GeoApiContext;
import com.google.maps.TimeZoneApi;
import com.google.maps.model.LatLng;

public class CoordinatesToTime implements Function<LatLng, Optional<TimeZone>> {
	
	//Personal google api key
	private static final String APP_KEY = "AIzaSyDjzGxvioRbuIiah6JleVocmLvbe2UNCUM";
	private static GeoApiContext context = new GeoApiContext().setApiKey(APP_KEY);

	@Override
	public Optional<TimeZone> apply(LatLng coordinates) {
		Optional<TimeZone> timezone = Optional.empty();
		
		try {
			timezone = Optional.of(TimeZoneApi.getTimeZone(context, coordinates).await());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return timezone;
	}
}
