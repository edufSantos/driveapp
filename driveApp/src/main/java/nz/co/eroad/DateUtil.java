package nz.co.eroad;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {

	/**
	 * Gets the UTC date time 
	 * @return DateTimeFormatter
	 */
	private static DateTimeFormatter getUTCDateTimeFormat() {
		return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneOffset.UTC);
	}
	
	/**
	 * Get date from UTC
	 * @param dateTime
	 * @return
	 */
	public static LocalDateTime getLocalDateFromUtc(String dateTime) {
		final DateTimeFormatter UTCTimeFormat = getUTCDateTimeFormat();
		final ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateTime, UTCTimeFormat);
		return LocalDateTime.ofInstant(zonedDateTime.toInstant(), ZoneId.of("Pacific/Auckland"));
	}
}
